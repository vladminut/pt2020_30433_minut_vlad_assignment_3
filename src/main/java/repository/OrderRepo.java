package repository;

import access.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderRepo extends SQLException {
    public static  String createOrder = "insert into orders (client, product, quantity) " +
            " values (?,?,?);";

    public static void addOrder(String c, String p, int q){

        String decrementproduct = "if quantity>= ? then Update product set quantity = quantity - ? where name = ? end if";
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        PreparedStatement findStatement2 = null;
        String selectQuantity = "Select product.quantity from product";
        PreparedStatement findStatement3 = null;
        int quantity=0;
        try{
            findStatement3 = dbConnection.prepareStatement(selectQuantity);
            ResultSet rs = findStatement3.executeQuery();
            while(rs.next())
             quantity = rs.getInt("quantity");
            findStatement = dbConnection.prepareStatement(createOrder);
            findStatement.setString(1,c);
            findStatement.setString(2,p);
            findStatement.setInt(3,q);


            findStatement2 = dbConnection.prepareStatement(decrementproduct);
            findStatement2.setInt(1,q);
            findStatement2.setInt(2,q);
            findStatement2.setString(3, p);
            if(q<=quantity)
            {
                findStatement.executeUpdate();
                findStatement2.executeUpdate();

            }
            else System.out.println("nu sunt destule produse");
        }
        catch (SQLException exception)
        {
            return;
        }
    }
}
