package repository;
import access.ConnectionFactory;
import model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductRepo extends SQLException {
    public static  String insertProduct = "insert into product (name, quantity, price) " +
            " values (?,?,?);";

    public static void insertProduct(String n, int q, float p){

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        try{
            findStatement = dbConnection.prepareStatement(insertProduct);
            findStatement.setString(1,n);
            findStatement.setInt(2,q);
            findStatement.setFloat(3,p);
            findStatement.executeUpdate();
        }
        catch (SQLException exception)
        {
            return;
        }
    }

    public static String deleteProductByName = "delete from product where name=?;";

    public static void deleteProductByName(String n){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        try {
            findStatement = dbConnection.prepareStatement(deleteProductByName);
            findStatement.setString(1, n);
            findStatement.executeUpdate();
        }
        catch (SQLException exception)
        {
            return;
        }
    }

    public static String findProductByName = "SELECT * from product where product.name = ?;";

    public static Product findProductByName(String n)
    {

        Product product = new Product();

        try {
            Connection dbConnection = ConnectionFactory.getConnection();
            PreparedStatement findStatement = dbConnection.prepareStatement(findProductByName);
            findStatement.setString(1, n);
            ResultSet rs = findStatement.executeQuery();
            while(rs.next()){


            product.setName(rs.getString("name"));
            product.setQuantity(rs.getInt("quantity"));
            product.setPrice(rs.getFloat("price"));
            return product;


        }
            return null;
        }
        catch (SQLException exception)
        {
            exception.printStackTrace();
            return null;
        }
    }



}

