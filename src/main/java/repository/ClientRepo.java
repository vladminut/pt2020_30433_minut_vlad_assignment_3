package repository;

import access.ConnectionFactory;
import model.Client;

import java.sql.*;

public class ClientRepo extends SQLException {

    public static  String insertClient = "insert into client (name, address) " +
                                         " values (?,?);";

    public static void insertClient(String n, String a){

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        try{
            findStatement = dbConnection.prepareStatement(insertClient);
            findStatement.setString(1,n);
            findStatement.setString(2,a);
            findStatement.executeUpdate();
        }
        catch (SQLException exception)
        {
            return;
        }
    }

    public static String deleteClientByName = "delete from client where name=?;";
    public static void deleteClientByName(String n){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        try {
            findStatement = dbConnection.prepareStatement(deleteClientByName);
            findStatement.setString(1, n);
            findStatement.executeUpdate();
        }
        catch (SQLException exception)
        {
            return;
        }
    }

}
