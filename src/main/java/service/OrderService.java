package service;

import repository.OrderRepo;


public class OrderService {
    public void insertOrder(String clientName, String productName, int quantity)
    {
        OrderRepo.addOrder(clientName, productName, quantity);
    }
}
