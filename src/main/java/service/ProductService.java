package service;
import model.Product;
import repository.ProductRepo;

public class ProductService {

    ProductRepo productRepo = new ProductRepo();

    public void addProduct(String name, int quantity, float price)
    {
        productRepo.insertProduct(name, quantity, price);
    }

    public void deleteProduct(String name)
    {
        productRepo.deleteProductByName(name);
    }

    public Product findProductByName(String name)
    {
        return productRepo.findProductByName(name);
    }
}
