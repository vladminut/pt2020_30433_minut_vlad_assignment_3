package service;
import repository.ClientRepo;

public class ClientService {

    ClientRepo clientRepo = new ClientRepo();

    public void addClient(String name, String address)
    {
            clientRepo.insertClient(name, address);
    }
    public void deleteClient(String name)
    {
        clientRepo.deleteClientByName(name);
    }

}
