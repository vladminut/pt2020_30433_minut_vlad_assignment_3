package service;

import presentation.CreateBillPDF;
import presentation.CreateClientsPDF;
import presentation.CreateOrderPDF;
import presentation.CreateProductPDF;

public class ReadLogic {

    public void read(String controlString, String dataString, String pdfName)
    {
        if(controlString.equals("Report client"))
        {
            CreateClientsPDF createClientsPDF = new CreateClientsPDF();
            createClientsPDF.makeClientsPDF();
        }
        if(controlString.equals("Report product"))
        {
            CreateProductPDF createProductPDF = new CreateProductPDF();
            createProductPDF.makeProductPDF();
        }
        if(controlString.equals("Report order"))
        {
            CreateOrderPDF createOrderPDF = new CreateOrderPDF();
            createOrderPDF.makeOrderPDF();
        }

        if(controlString.equals("Insert client"))
        {
            ClientService clientService = new ClientService();
            String[] array = dataString.split(",");

            clientService.addClient(array[0], array[1]);

        }

        if(controlString.equals("Delete client"))
        {
            ClientService clientService = new ClientService();
            String[] array = dataString.split(",");

            clientService.deleteClient(array[0]);
        }

        if(controlString.equals("Insert product"))
        {
            ProductService productService = new ProductService();
            String [] array = dataString.split(",");
            productService.addProduct(array[0],Integer.parseInt(array[1]),Float.parseFloat(array[2]));
        }

        if(controlString.equals("Delete product"))
        {
            ProductService productService = new ProductService();
            String [] array = dataString.split(",");
            productService.deleteProduct(array[0]);
        }

        if(controlString.equals("Order"))
        {
            OrderService orderService = new OrderService();
            String[] array = dataString.split(",");
            orderService.insertOrder(array[0],array[1], Integer.parseInt(array[2]));
            CreateBillPDF createBillPDF = new CreateBillPDF();
            createBillPDF.makeBill(pdfName);

        }
    }
}
