package presentation;
import service.ReadLogic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class ReadTextfile {

    public void Read(){
        try {
            File myObj = new File("textfile.txt");
            String pdfName = "";
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] arr = data.split(":");
                pdfName +='.';
                ReadLogic readLogic = new ReadLogic();
                if(arr.length == 1) readLogic.read(arr[0], null, pdfName);
                else readLogic.read(arr[0], arr[1], pdfName);
            }
            myReader.close();
        } catch (FileNotFoundException e)

    {
        System.out.println("An error occurred");
        e.printStackTrace();
    }

    }
}

