package presentation;

import access.ConnectionFactory;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CreateOrderPDF {

    public void makeOrderPDF() {
        try {
            String name = "OrdersReport.pdf";
            String selectAllOrders = "select * from orders";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(name));
            document.open();
            Connection dbConnection = ConnectionFactory.getConnection();
            PreparedStatement statement = null;
            ResultSet rs = null;
            statement = dbConnection.prepareStatement(selectAllOrders);
            rs = statement.executeQuery();


            PdfPTable table = new PdfPTable(3);
            PdfPCell cell1 = new PdfPCell(new Phrase("Client Name"));
            PdfPCell cell2 = new PdfPCell(new Phrase("Product Name"));
            PdfPCell cell3 = new PdfPCell(new Phrase("Quantity"));
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.setHeaderRows(1);

            while(rs.next()){
                table.addCell(rs.getString("client"));
                table.addCell(rs.getString("product"));
                table.addCell(rs.getString("quantity"));
            }
            document.add(table);
            document.close();
        }
        catch (Exception exception)
        {
            System.out.println("error creating order pdf");
        }
    }
}
