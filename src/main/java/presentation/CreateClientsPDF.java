package presentation;

import access.ConnectionFactory;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CreateClientsPDF {

    public void makeClientsPDF() {
        try {
            String name ="ClientsReport.pdf";
            String selectAllClients = "Select * from client";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(name));
            document.open();
            Connection dbConnection = ConnectionFactory.getConnection();
            PreparedStatement statement = null;
            ResultSet rs = null;
            statement = dbConnection.prepareStatement(selectAllClients);
            rs = statement.executeQuery();


            PdfPTable table = new PdfPTable(2);
            PdfPCell cell1 = new PdfPCell(new Phrase("Client Name"));
            PdfPCell cell2 = new PdfPCell(new Phrase("Address"));
            table.addCell(cell1);
            table.addCell(cell2);
            table.setHeaderRows(1);

            while(rs.next()){
                table.addCell(rs.getString("name"));
                table.addCell(rs.getString("address"));
            }
            document.add(table);
            document.close();
        }
        catch (Exception exception)
        {
            System.out.println("error creating client pdf");
        }
    }
}