package presentation;

import access.ConnectionFactory;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CreateProductPDF {
    public void makeProductPDF() {
        try {
            String name = "ProductReport.pdf";
            String selectAllProducts = "Select * from product";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(name));
            document.open();
            Connection dbConnection = ConnectionFactory.getConnection();
            PreparedStatement statement = null;
            ResultSet rs = null;
            statement = dbConnection.prepareStatement(selectAllProducts);
            rs = statement.executeQuery();


            PdfPTable table = new PdfPTable(3);
            PdfPCell cell1 = new PdfPCell(new Phrase("Product Name"));
            PdfPCell cell2 = new PdfPCell(new Phrase("Quantity"));
            PdfPCell cell3 = new PdfPCell(new Phrase("Price"));
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.setHeaderRows(1);

            while(rs.next()){
                table.addCell(rs.getString("name"));
                table.addCell(rs.getString("quantity"));
                table.addCell(rs.getString("price"));
            }
            document.add(table);
            document.close();
        }
        catch (Exception exception)
        {
            System.out.println("error creating product pdf");
        }
    }
}
