package presentation;

import access.ConnectionFactory;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import model.Product;
import service.ProductService;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CreateBillPDF {

    public void makeBill(String pdfName) {


        float productPrice = 0;


        try {
            String selectAllOrders = "SELECT * FROM orders" +
                                    " ORDER BY id DESC" +
                                     " LIMIT 1";

            Connection dbConnection = ConnectionFactory.getConnection();

            PreparedStatement statement = dbConnection.prepareStatement(selectAllOrders);
            ResultSet rs = statement.executeQuery();
            String name = "Bill"+pdfName+".pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(name));
            document.open();
            PdfPTable table = new PdfPTable(3);
            PdfPCell cell1 = new PdfPCell(new Phrase("Client Name"));
            PdfPCell cell2 = new PdfPCell(new Phrase("Product Name"));
            PdfPCell cell3 = new PdfPCell(new Phrase("Quantity"));
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.setHeaderRows(1);

                while(rs.next()) {

                table.addCell(rs.getString("client"));
                String productName = rs.getString("product");
                table.addCell(productName);
                ProductService productService = new ProductService();
                Product product = new Product();

                product = productService.findProductByName(productName);
                if(product == null)
                {
                    System.out.println("nu exista produsul!");
                    return;
                }
                int productQuantity = rs.getInt("quantity");
                table.addCell(String.valueOf(productQuantity));
                try {
                    productPrice = productQuantity * product.getPrice();
                }
                catch (Exception e){
                    e.printStackTrace();
                }



            document.add(table);
            Paragraph para = new Paragraph(String.valueOf(productPrice));
            document.add(para);
            document.close();
        }}
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
