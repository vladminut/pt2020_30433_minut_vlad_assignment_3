package access;
import java.sql.*;
import java.util.logging.Logger;

public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/tema3";
    private static final String USER = "root";
    private static final String PASS = "1234";

    private  static ConnectionFactory singleInstance = new ConnectionFactory();

    public static Connection getConnection(){
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(DBURL, USER, PASS);
        }
        catch (SQLException exception)
        {
            System.out.println("nu s-a realizat conexiunea");
            exception.printStackTrace();
            return null;
        }
        catch (ClassNotFoundException exception)
        {
            System.out.println("nu s-a realizat conexiunea");
            return null;
        }
    }

    public static void close(Connection connection){
        if(connection!=null)
        try {
            connection.close();
        }
        catch (SQLException exception)
        {
            System.out.println("nu s-a oprit conexiunea");
        }
    }

    public static void close(Statement statement) {
        if (statement != null)
            try {
                statement.close();
            } catch (SQLException exception)
            {
                System.out.println("nu s-a oprit statementul");
            }
    }

    public static void close(ResultSet resultSet){
        if(resultSet!=null)
            try {
                resultSet.close();
            }
        catch (SQLException exception)
        {
            System.out.println("nu s-a oprit result setul");
        }
    }
}
