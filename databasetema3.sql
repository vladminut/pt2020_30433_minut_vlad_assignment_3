-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tema3
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tema3
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tema3` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `tema3` ;

-- -----------------------------------------------------
-- Table `tema3`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tema3`.`orders` (
  `client` VARCHAR(45) NOT NULL,
  `product` VARCHAR(45) NOT NULL,
  `quantity` INT NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `client_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `client`),
  INDEX `fk_orders_client1_idx` (`client_name` ASC) VISIBLE,
  CONSTRAINT `fk_orders_client1`
    FOREIGN KEY (`client_name`)
    REFERENCES `tema3`.`client` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tema3`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tema3`.`product` (
  `name` VARCHAR(45) NOT NULL,
  `quantity` INT NOT NULL,
  `price` FLOAT NOT NULL,
  `orders_id` INT NOT NULL,
  `orders_client` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`name`, `orders_id`, `orders_client`),
  INDEX `fk_product_orders_idx` (`orders_id` ASC, `orders_client` ASC) VISIBLE,
  CONSTRAINT `fk_product_orders`
    FOREIGN KEY (`orders_id` , `orders_client`)
    REFERENCES `tema3`.`orders` (`id` , `client`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tema3`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tema3`.`client` (
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `product_name` VARCHAR(45) NOT NULL,
  `product_orders_id` INT NOT NULL,
  `product_orders_client` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`name`),
  INDEX `fk_client_product1_idx` (`product_name` ASC, `product_orders_id` ASC, `product_orders_client` ASC) VISIBLE,
  CONSTRAINT `fk_client_product1`
    FOREIGN KEY (`product_name` , `product_orders_id` , `product_orders_client`)
    REFERENCES `tema3`.`product` (`name` , `orders_id` , `orders_client`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
